# -*- coding: utf-8 -*-
import re

from django.forms import CharField, ModelForm, ValidationError, IntegerField
from django.utils.translation import ugettext_lazy as _

from models import Profile, ActivationCode

from api.tools import normalize_number, normalize_phone, validate_mobile, is_valid_ssn, allow_get_code


class CreateActivationCodeForm(ModelForm):
    class Meta:
        model = ActivationCode
        fields = ('phone',)

    phone = CharField(max_length=12, required=True)

    def clean(self):
        cleaned_data = super(CreateActivationCodeForm, self).clean()
        phone = cleaned_data.get('phone', None)
        if phone is None:
            raise ValidationError({'phone': [_("This field is necessary.")]})

        ph = normalize_number(phone)
        norm_phone = normalize_phone(ph)
        cleaned_data["phone"] = norm_phone
        if not validate_mobile(norm_phone):
            raise ValidationError({'phone': [_("The entered phone number is not valid")]})

        # Check attempts count
        is_allow, count, msg = allow_get_code(norm_phone)
        if not is_allow:
            raise ValidationError({'phone': [msg]})
        cleaned_data['count'] = count

        return cleaned_data


class VerifyCodeFrom(ModelForm):
    class Meta:
        model = ActivationCode
        fields = ('phone', 'code')

    phone = CharField(max_length=12, required=True)
    code = IntegerField(required=True)

    def clean(self):
        cleaned_data = super(VerifyCodeFrom, self).clean()
        phone = cleaned_data.get('phone', None)
        code = str(cleaned_data.get('code', None))

        if phone is None:
            raise ValidationError({'phone': [_("This field is necessary.")]})

        if code is None:
            raise ValidationError({'code': [_("This field is necessary.")]})

        # Normalize phone number
        ph = normalize_number(phone)
        norm_phone = normalize_phone(ph)
        norm_code = normalize_number(code)

        if not validate_mobile(norm_phone):
            raise ValidationError({'phone': [_("The entered phone number is not valid")]})

        if not re.search(r'^\d{4}$', norm_code):
            raise ValidationError({'code': [_('Please enter your code correctly.')]})

        cleaned_data["phone"] = norm_phone
        cleaned_data["code"] = norm_code
        return cleaned_data


class RegisterForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'ssn', 'phone')

    first_name = CharField(max_length=50,
                           min_length=3,
                           required=True)
    last_name = CharField(max_length=50,
                          min_length=3,
                          required=True)
    phone = CharField(max_length=12, required=True)
    ssn = CharField(max_length=10, required=True)

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        phone = cleaned_data.get('phone')
        ssn = cleaned_data.get('ssn')

        if phone is None:
            raise ValidationError({'phone': [_("This field is necessary . ")]})

        # Check parameters
        message = check_string(str(first_name))
        if message is not None:
            raise ValidationError({'first_name': [_("The first name must be between 3 and 50 characters and can only include Persian and English and _.")]})

        message = check_string(str(last_name))
        if message is not None:
            raise ValidationError({'last_name': [_('The last name must be between 3 and 50 characters and can only include Persian and English and _.')]})

        # Checking phone
        ph = normalize_number(phone)
        norm_phone = normalize_phone(ph)
        if not validate_mobile(norm_phone):
            raise ValidationError({'phone': [_('Please enter your phone number correctly')]})

        if Profile.objects.filter(phone=norm_phone).exists():
            raise ValidationError({'phone': [_('A user with this phone number has already registered in the system.')]})

        # checking ssn
        normalize_ssn = normalize_number(ssn)
        if not re.search(r'^\d{10}$', normalize_ssn) or not is_valid_ssn(normalize_ssn):
            raise ValidationError({'ssn': [_('Please enter your ssn correctly.')]})

        if Profile.is_exist_ssn(normalize_ssn):
            raise ValidationError({'ssn': [_('A user with this ssn has already registered in the system.')]})

        if Profile.is_exist_phone(phone=norm_phone):
            raise ValidationError({'phone': [_('A user with this phone has already registered in the system.')]})

        cleaned_data["phone"] = norm_phone
        cleaned_data["ssn"] = normalize_ssn
        return cleaned_data


class UpdateProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'birthdate', 'gender')

    first_name = CharField(max_length=50,
                           min_length=3,
                           required=True)
    last_name = CharField(max_length=50,
                          min_length=3,
                          required=True)

    def clean(self):
        cleaned_data = super(UpdateProfileForm, self).clean()
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        birthdate = cleaned_data.get('birthdate')
        gender = cleaned_data.get('gender')

        # Check parameters
        message = check_string(str(first_name))
        if message is not None:
            raise ValidationError({'first_name': [_("The first name must be between 3 and 50 characters and can only include Persian and English and _.")]})

        message = check_string(str(last_name))
        if message is not None:
            raise ValidationError({'last_name': [_('The last name must be between 3 and 50 characters and can only include Persian and English and _.')]})

        message = check_birthdate(str(birthdate))
        if message is not None:
            raise ValidationError({'birthdate': [_('Please enter a valid birth date ')]})

        # Checking gender
        if gender not in [0, 1, 2, 10]:
            raise ValidationError({'gender': [_('Please enter your gender correctly.')]})

        return cleaned_data


def check_string(string):
    message = None
    new_string = re.match("^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤآإأءًٌٍَُِa-zA-Z_\.! ]{3,50}$", string)
    if new_string is None or new_string.group() != string:
        message = _('bad characters')
    return message


def check_birthdate(string):
    message = None
    new_string = re.search(r'^\d{10}$', string)
    if new_string is None or new_string.group() != string:
        message = _('bad characters')
    return message


class UpdateAvatarForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('avatar',)
