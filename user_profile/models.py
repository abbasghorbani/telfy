# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time
import os
import uuid
from datetime import datetime

# from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.core.cache import cache
from django.db.models import F
# from django.core.mail import send_mail
from django.core import serializers
from django.db import models
from django.db.models.signals import post_save

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit

from api.tools import simple_user_json, send_sms, random_with_N_digits


def avatar_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    avatars_prefix = "avatars/"
    return '/'.join([avatars_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


class Profile(models.Model):
    class Meta:
        db_table = 'profile'
        verbose_name_plural = _("profiles")
        verbose_name = _("profile")

    NOTSET = 0
    NOTMARRIED = 1
    MARRIED = 2
    OTHER = 10

    MARRIED_TYPE = (
        (NOTSET, _('not set')),
        (NOTMARRIED, _('not married')),
        (MARRIED, _('married')),
    )

    MALE = 1
    FEMALE = 2
    GENDER = (
        (NOTSET, _('not set')),
        (MALE, _('male')),
        (FEMALE, _('female')),
        (OTHER, _('other')),
    )

    first_name = models.CharField(max_length=50, verbose_name=_("First name"), null=True, blank=True)
    last_name = models.CharField(max_length=50, verbose_name=_("Last name"), null=True, blank=True)
    avatar = ProcessedImageField(verbose_name=_("Avatar"), upload_to=avatar_file_name, processors=[ResizeToFit(236)], format='JPEG', options={'quality': 70})
    user = models.OneToOneField(User, verbose_name=_("User"))
    credit = models.IntegerField(default=0, verbose_name=_("Credit"))
    birthdate = models.IntegerField(verbose_name=_("‌‌Birthdate"), default=0)
    gender = models.IntegerField(verbose_name=_('Gender'), choices=GENDER, default=NOTSET)
    phone = models.CharField(max_length=12, verbose_name=_("Phone"), db_index=True, null=True)
    ssn = models.CharField(max_length=10, verbose_name=_("SSN"), db_index=True, null=True)

    def get_json(self, fields=None, exclude=None):

        def need_fields(profile_object):

            final_object = {}
            if fields:
                final_object = {
                    field: profile_object[field] for field in fields
                }
            else:
                final_object = profile_object

            if exclude:
                if not fields:
                    final_object = profile_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = {}
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['user'] = simple_user_json(user=self.user)

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        data = need_fields(data)
        return data

    @classmethod
    def inc_credit(cls, user_id, amount):
        from api.auth_cache import AuthCache
        from invoice.models import CreditLog
        Profile.objects.filter(user_id=user_id)\
            .update(credit=F('credit') + amount)

        # Add log into creditLog model
        CreditLog.create_credit_log(user_id=user_id, amount=amount,
                                    status=CreditLog.COMPLETED, mode=CreditLog.INCREASE)

        AuthCache.remove_profile_by_id(user_id=user_id)

    @classmethod
    def dec_credit(cls, user_id, amount):
        from api.auth_cache import AuthCache
        from invoice.models import CreditLog
        profile = AuthCache.get_profile_from_id(user_id)

        try:
            if profile["credit"] - amount < 0:
                Profile.objects.filter(id=profile["id"]).update(credit=0)
            else:
                Profile.objects.filter(id=profile["id"]).update(credit=F('credit') - amount)
        except Exception as e:
            print str(e), "model user_profile: dec_credit function"
            return False

        # Add log into creditLog model
        CreditLog.create_credit_log(user_id=user_id, amount=amount,
                                    status=CreditLog.COMPLETED, mode=CreditLog.DECREASE)

        AuthCache.remove_profile_by_id(user_id=user_id)
        return True

    @classmethod
    def is_exist_ssn(cls, ssn):
        return cls.objects.filter(ssn__iexact=ssn).exists()

    @classmethod
    def is_exist_phone(cls, phone):
        return cls.objects.filter(phone__iexact=phone).exists()


class ApiKey(models.Model):
    class Meta:
        db_table = 'api_key'
        verbose_name_plural = _("ApiKey")
        verbose_name = _("ApiKey")

    user = models.ForeignKey(User, verbose_name=_("User"))
    token = models.CharField(max_length=255, unique=True, verbose_name=_("Token"))
    create_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Create at"))

    def get_json(self):

        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data

    @classmethod
    def create_token(cls, sender, instance, *args, **kwargs):
        if kwargs['created']:
            key_exists = True
            key = None
            while key_exists:
                key = uuid.uuid4().hex
                key_exists = cls.objects.filter(token=key).exists()
                if not key_exists:
                    try:
                        cls.objects.create(user=instance, token=key)
                    except Exception as e:
                        print str(e)

    @classmethod
    def rebuild_token(cls, user):
        key_exists = True
        key = None
        while key_exists:
            key = uuid.uuid4().hex
            key_exists = cls.objects.filter(token=key).exists()
            if not key_exists:
                try:
                    apikey = cls.objects.create(user=user, token=key)
                    return apikey
                except Exception as e:
                    print str(e)
        return False

    @classmethod
    def update_token(cls, user_id):
        key_exists = True
        key = None
        while key_exists:
            key = uuid.uuid4().hex
            key_exists = cls.objects.filter(token=key).exists()
            if not key_exists:
                try:
                    cls.objects.filter(user_id=user_id).update(token=key)
                    return key
                except Exception as e:
                    print str(e)
        return None


class ActivationCode(models.Model):
    class Meta:
        db_table = 'activation_code'
        verbose_name_plural = _("ActivationCodes")
        verbose_name = _("ActivationCode")

    # user = models.ForeignKey(User, verbose_name=_("User"))
    phone = models.CharField(max_length=12, verbose_name=_("phone"), unique=True)
    code = models.CharField(max_length=255, unique=True, verbose_name=_("Code"))
    create_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Create at"))
    is_active = models.BooleanField(default=False, verbose_name=_("is active"))

    @classmethod
    def create_code(cls, phone, count, resend=False):
        key_exists = True
        code = None

        try:
            exists_phone = cls.objects.only('is_active', 'code').get(phone=phone)
            if not exists_phone.is_active:
                # Send SMS
                send_sms(phone, exists_phone.code, resend)
                # Set attempts user count
                timestamp = datetime.now().strftime("%s")
                count += 1
                new_string = "{}:{}".format(count, timestamp)
                cache.set(phone, new_string)
                return True

            # Remove activation code if is_active=True
            exists_phone.delete()
        except cls.DoesNotExist:
            pass
        except Exception as e:
            print str(e), "|| ActivationCode model, create_code function"
            return False

        while key_exists:
            code = "{}".format(random_with_N_digits(4))
            key_exists = cls.objects.filter(code=code).exists()
            if not key_exists:
                try:
                    cls.objects.create(phone=phone, code=code)
                    # Send SMS
                    send_sms(phone, code, resend)
                    # Set attempts user count
                    timestamp = datetime.now().strftime("%s")
                    count += 1
                    new_string = "{}:{}".format(count, timestamp)
                    cache.set(phone, new_string)
                except Exception as e:
                    msg = "|| ActivationCode model, create_code function"
                    print str(e), msg
                    return False
        return True


class PhoneData(models.Model):
    class Meta:
        db_table = 'phone_Data'
        verbose_name_plural = _("phone_Datas")
        verbose_name = _("phone_Data")
    user = models.ForeignKey(User, verbose_name=_("User"))
    hashcode = models.CharField(max_length=50, verbose_name=_("Hashcode"), db_index=True)
    extra_data = models.TextField(verbose_name=_("Extra data"), default="")
    push_token = models.CharField(max_length=500, verbose_name=_("Push token"))
    logout = models.BooleanField(default=False, verbose_name=_("Logout"))

    @classmethod
    def update_phone_data(cls, user_id, data):
        import json
        try:
            json_data = json.loads(data)
            hashcode = json_data["hashcode"]
        except Exception as e:
            print str(e)
            return

        try:
            obj = cls.objects.get(user_id=user_id)
            obj.extra_data = data
            obj.hashcode = hashcode
            obj.save()
        except cls.DoesNotExist:
            obj = cls.objects.create(user_id=user_id, hashcode=hashcode, extra_data=data)


post_save.connect(ApiKey.create_token, sender=User)
# post_save.connect(ActivationCode.create_code, sender=User)
