from django.contrib import admin
from models import Profile, ApiKey, ActivationCode


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name', 'avatar', 'credit', 'phone', 'ssn')
    search_fields = ['ssn', 'phone']
    raw_id_fields = ("user",)

    # def image_tag(self, obj):
    #     if obj.avatar:
    #         return '<img src="{}" />'.format(obj.avatar.url)
    #     else:
    #         return u'<img src="" />'
    # image_tag.short_description = 'Image'
    # image_tag.allow_tags = True


class ApiKeyAdmin(admin.ModelAdmin):
    list_display = ('user', 'token', 'create_at')
    search_fields = ['user__username', 'user__id']
    raw_id_fields = ("user",)


class ActivationCodeAdmin(admin.ModelAdmin):
    list_display = ('code', 'phone', 'create_at', "is_active")
    search_fields = ['phone']


admin.site.register(Profile, ProfileAdmin)
admin.site.register(ApiKey, ApiKeyAdmin)
admin.site.register(ActivationCode, ActivationCodeAdmin)
