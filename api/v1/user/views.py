# -*- coding: utf-8 -*-
import os

from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
from django.http import UnreadablePostError
from django.conf import settings

from api.decorators import check_user_auth, method_is_post
from api.http_response import return_json_data, bad_request_response, not_found_response, forbidden_response

from user_profile.models import Profile
from user_profile.forms import UpdateProfileForm, UpdateAvatarForm
from api.auth_cache import AuthCache


@check_user_auth
def user_profile(request):
    cur_user = request.CUR_USER
    pro_json = AuthCache.get_profile_from_id(user_id=cur_user.id)
    return return_json_data(data={'profile': pro_json})


@csrf_exempt
@check_user_auth
@method_is_post
def update_profile(request):

    profile = None
    current_user_id = request.CUR_USER.id

    try:
        profile = Profile.objects.get(user_id=current_user_id)
    except Exception:
        return not_found_response(message=_('Profile not found'))

    if profile.user_id != current_user_id:
        return forbidden_response(message=_("You are not allowed to change this profile."))

    try:
        form = UpdateProfileForm(request.POST, instance=profile)
    except UnreadablePostError:
        return bad_request_response()

    if form.is_valid():
        model = form.save(commit=False)
        model.save()
        data = model.get_json()
        return return_json_data(data=data)
    else:
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        msg = {field: message[0]}
        return bad_request_response(message=msg)


@csrf_exempt
@check_user_auth
@method_is_post
def update_avatar(request):

    current_user_id = request.CUR_USER.id
    try:
        profile = Profile.objects.get(user_id=current_user_id)
    except Exception:
        return not_found_response(message=_('Profile not found'))

    if profile.user_id != current_user_id:
        return forbidden_response(message=_("You are not allowed to change this profile."))

    old_avatar = os.path.join(settings.MEDIA_ROOT, profile.avatar.name)

    try:
        form = UpdateAvatarForm(request.POST, request.FILES, instance=profile)
    except UnreadablePostError:
        return bad_request_response()

    if form.is_valid():
        model = form.save(commit=False)
        model.save()
        try:
            os.remove(old_avatar)
        except Exception:
            pass
        data = model.get_json()
        return return_json_data(data=data)
    else:
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        msg = {field: message[0]}
        return bad_request_response(message=msg)


@check_user_auth
def remove_avatar(request):
    current_user_id = request.CUR_USER.id
    try:
        profile = Profile.objects.get(user_id=current_user_id)
    except Exception:
        return not_found_response(message=_('Profile not found'))

    if profile.user_id != current_user_id:
        return forbidden_response(message=_("You are not allowed to change this profile."))

    profile.avatar.delete()
    return return_json_data(data=profile.get_json())
