# -*- coding: utf-8 -*-

from django.conf.urls import url
from api.v1.user import views

urlpatterns = [
    url(r'profile/$', views.user_profile, name='api-v1-user-profile'),
    url(r'profile/update/$', views.update_profile, name='api-v1-user-profile-update'),
    url(r'updateAvatar/$', views.update_avatar, name='api-v1-user-update-avatar'),
    url(r'removeAvatar/$', views.remove_avatar, name='api-v1-user-remove-avatar'),

]
