from django.conf.urls import url, include

urlpatterns = [
    url(r'^auth/', include('api.v1.authentication.urls')),
    url(r'^user/', include('api.v1.user.urls')),
]
