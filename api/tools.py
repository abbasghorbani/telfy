# -*- coding: utf-8 -*-
import urllib
import re
import os
import sys
import random
import time
import string
import json
from time import mktime
from datetime import datetime, timedelta
from PIL import Image

from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.utils.translation import ugettext as _

QUARTER = 15
HALF = 30
HOUR = 60
HOUR_HALF = 45

reload(sys)
sys.setdefaultencoding('utf-8')


def media_abs_url(url):
    if url.startswith('http://'):
        new_url = url
    else:
        new_url = settings.API_URL + url
    return new_url


def simple_user_json(user, fields=None, exclude=None):
    if isinstance(user, (int, long)):
        try:
            user = User.objects.get(id=user)
        except Exception:
            print "User Not Found"
            user = None

    def need_fields(user_object):

        final_object = {}
        if fields:
            final_object = {field: user_object[field] for field in fields}
        else:
            final_object = user_object

        if exclude:
            if not fields:
                final_object = user_object

            for field in exclude:
                try:
                    final_object.pop(field)
                except Exception:
                    pass

        return final_object

    data = {}
    if user:
        serialized_obj = serializers\
            .serialize('python', [user], fields=('username', 'id'))

        serialized_obj[0]['fields']['id'] = serialized_obj[0]['pk']

        data = serialized_obj[0]['fields']
        avatar = user.profile.avatar
        data['avatar'] = ""
        if avatar:
            data['avatar'] = media_abs_url(avatar.url)
        data = need_fields(data)

    else:
        data = []
    return data


def abs_url(url, api=True):
    if not url.startswith('http://'):
        if api:
            return settings.SITE_URL + url
        else:
            return settings.SITE_URL + url


def get_next_url(url_name, offset=None, url_args={}, **kwargs):
    n_url_p = reverse(url_name, kwargs=url_args)
    d = {}
    if offset:
        d['offset'] = offset
    for k, v in kwargs.iteritems():
        d[k] = v
    if d:
        n_url_p = n_url_p + "?" + urllib.urlencode(d)
    return abs_url(n_url_p)


def normalize_phone(number):
    """
    convert   09195308965 -> 989195308965
            +989195308965 -> 989195308965
           00989195308965 -> 989195308965
    """

    if number.startswith("00"):
        number = number.replace("00", "98")

    elif number.startswith("0"):
        number = number.replace("0", "98", 1)

    elif number.startswith("+"):
        number = number.replace("+", "")

    return str(int(float(number)))


def validate_mobile(value):
    status = False
    rule = re.compile(r'^\+?(98)\d{10}$')
    if rule.search(value):
        status = True
    return status


def save_thumb(file_path, basewidth):
    idir = os.path.dirname(file_path)
    iname = os.path.basename(file_path)
    img = Image.open(file_path)

    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((basewidth, hsize), Image.ANTIALIAS)
    w, h = img.size

    file_name = "{}x{}_{}".format(w, h, iname)
    split_path = idir.split("user")
    thumb_path = "{}user/thumb{}".format(split_path[0], split_path[1])

    image_path = "{}/{}".format(thumb_path, file_name)

    if not os.path.exists(thumb_path):
        try:
            os.makedirs(thumb_path, mode=0777)
        except Exception, e:
            print str(e)

    if not os.path.exists(image_path):
        img.save(image_path)

    file_path = "/media/user/thumb{}/{}".format(split_path[1], file_name)

    return image_path, file_path, w, h


def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return random.randint(range_start, range_end)


def normalize_number(value):
    value = value.replace('۱', '1')
    value = value.replace('۲', '2')
    value = value.replace('۳', '3')
    value = value.replace('۴', '4')
    value = value.replace('۵', '5')
    value = value.replace('۶', '6')
    value = value.replace('۷', '7')
    value = value.replace('۸', '8')
    value = value.replace('۹', '9')
    value = value.replace('۰', '0')
    return value


def allow_get_code(phone):
    count = 0
    message = ""
    val = cache.get(phone)
    if not val:
        return True, count, message

    string = val.split(":")
    count = int(string[0])
    last_time = string[1]

    # Check attempts count
    if count >= settings.ATTEMPTS_COUNT:
        message = _("Too many attempts please try later")
        return False, count, message

    # Check attempts time
    last = datetime.fromtimestamp(int(last_time))
    now = datetime.now()
    diff = now - last
    if diff.seconds < settings.SLEEP_TIME:
        message = _("Please try 5 minutes later")
        return False, count, message

    return True, count, message


def is_valid_ssn(ssn):
    check = int(ssn[9])
    s = sum([int(ssn[x]) * (10 - x) for x in range(9)]) % 11
    return (s < 2 and check == s) or (s >= 2 and check + s == 11)


def send_sms(phone, message, resend=False):
    return
    # if phone.startswith("98"):
    #     phone = phone[2:]

    # if resend:
    #     url = "http://api.payamak-panel.com/post/send.asmx/SendSimpleSMS2"
    #     data = {'username': 's.adminy',
    #             'password': '6952',
    #             'from': '10002166551301',
    #             'to': phone,
    #             'text': message,
    #             'IsFlash': False}
    #     try:
    #         response = requests.post(url, data=data)
    #         print response.content

    #     except Timeout:
    #         print "request timeout"

    #     except ConnectionError:
    #         print "please check your connection"

    #     except HTTPError:
    #         print response.reason
    # else:
    #     url = "http://ip.sms.ir/SendMessage.ashx"
    #     data = {'user': '09159130625',
    #             'pass': '96311103',
    #             'lineNo': '30007723',
    #             'to': phone,
    #             'text': message}
    #     try:
    #         response = requests.get(url, params=data, timeout=10)
    #         print response.content

    #     except Timeout:
    #         print "request timeout"

    #     except ConnectionError:
    #         print "please check your connection"

    #     except HTTPError:
    #         print response.reason


def get_time_int(time_int, appointment_type=15, single=None):
    time_int = get_int(time_int)
    sum_start_minute = time_int * appointment_type
    sum_end_minute = sum_start_minute + appointment_type

    if sum_start_minute >= 1440 or sum_end_minute > 1440:
        print "error"
        if single is False:
            return '', ''
        return ''

    start_hour = (sum_start_minute) / 60
    start_minute = (sum_start_minute) - (start_hour * 60)

    end_hour = (sum_end_minute) / 60
    end_minute = (sum_end_minute) - (end_hour * 60)

    if start_hour < 10:
        start_hour = "0" + str(start_hour)

    if end_hour < 10:
        end_hour = "0" + str(end_hour)

    if end_hour == 24:
        end_hour = "23"
        end_minute = 59

    if start_minute < 10:
        start_minute = "0" + str(start_minute)

    if end_minute < 10:
        end_minute = "0" + str(end_minute)

    time_str = ""
    if single is None:
        time_str = "{}:{} - {}:{}".format(start_hour,
                                          start_minute,
                                          end_hour,
                                          end_minute)
    elif single == "start":
        time_str = "{}:{}".format(start_hour, start_minute)
    elif single == "end":
        time_str = "{}:{}".format(end_hour, end_minute)
    elif single is False:
        start = "{}:{}".format(start_hour, start_minute)
        end = "{}:{}".format(end_hour, end_minute)
        return start, end

    return time_str


def get_int(int_val):
    try:
        val = int(int_val)
    except Exception:
        val = 0
    return val


def get_next_weekday(startdate, weekday, datetime_format=True):
    """
    @startdate: given date, in format datetime
    @weekday: week day as a integer, between 0 (Monday) to 6 (Sunday)
        appointment.models.MONDAY
    # datetime.strptime('2013-05-27', '%Y-%m-%d')
    # datetime.today().strftime('%Y-%m-%d')
    """
    t = timedelta((7 + weekday - startdate.weekday()) % 7)
    if datetime_format:
        return (startdate + t)
    else:
        return (startdate + t).strftime('%Y-%m-%d')


def get_current_time_int(appointment_type=15, add_delay=0):
    now = datetime.today() + timedelta(minutes=add_delay)
    sum_minutes = (now.hour * 60) + now.minute
    time_int = sum_minutes / appointment_type
    return get_int(time_int)


def get_doctor(user_id, is_approve=True):
    from user_profile.models import Doctor
    try:
        doctor = Doctor.objects.get(user_id=user_id)
    except Exception:
        return False

    if not is_approve:
        return doctor

    if doctor.is_approve:
        return doctor

    return False


def get_type_time_int(appointment_type, start, end):
    diff = end - start
    if diff.days > 0:
        return []

    # diff_minute = ((diff.days * 86400) + diff.seconds) / 60

    start_hour = start.hour * 60
    start_minute = start.minute
    sum_start_minute = start_hour + start_minute
    start_time_int = sum_start_minute / appointment_type

    end_hour = end.hour * 60
    end_minute = end.minute
    sum_end_minute = end_hour + end_minute
    end_time_int = sum_end_minute / appointment_type

    time_int_list = []
    for time_int in range(start_time_int, end_time_int):
        time_int_list.append(time_int)
    return time_int_list


def is_valid_time_int(time_int, appointment_type):
    if (time_int * appointment_type) > 1440:
        return False
    return True


def get_other_time_int(time_int, appointment_type):
    if appointment_type == HOUR:
        other = time_int * 2
        return [other, other + 1]
    if appointment_type == HALF:
        other = time_int / 2
        return [other]


def prepare_data(payload):
    try:
        data = json.loads(payload)
    except Exception as e:
        print str(e), " || api tools.py: string_to_dict function"
        data = None

    if data is None:
        return None

    token = data.get('token', None)
    if token is None:
        return None

    packet_id = data.get('packet_id', None)
    if packet_id is None:
        return None

    packet_type = data.get('packet_type', None)
    if packet_type is None:
        return None

    message = data.get('payload', None)
    if message is None:
        return None

    return data


def get_packet_id(user_id=0):
    t = int(time.time())
    r = random.choice(range(60000, 80000))
    packet_id = "packet-{}-{}{}".format(user_id, t, r)

    return packet_id


def get_random_string(num=10):
    return ''.join(random.choice(string.ascii_uppercase +
                                 string.ascii_lowercase +
                                 string.ascii_letters) for _ in range(num))


def normalize_mongo_time_int(time_int_list, max_value):
    value_list = []
    for time_int in time_int_list:
        time_int = get_int(time_int)
        if time_int >= 0 and time_int <= max_value:
            value_list.append(time_int)
    value_list = list(set(value_list))
    value_list.sort()
    return value_list


def str2bool(v):
    return v.lower() in ("true", "True", "1")


def get_topic_obj(hashcode):
    from chat.models_mongo import Topic
    try:
        topic = Topic.objects.get(hashcode=hashcode)
    except Exception:
        topic = None

    return topic


def get_user_topic(user_id):
    from api.v1.chat.urls import API_VERSION
    topic = API_VERSION + "/user/{}/".format(user_id)
    return topic


def get_topic_uri(hashcode):
    from api.v1.chat.urls import API_VERSION
    topic = API_VERSION + "/topic/{}/".format(hashcode)
    return topic


def get_server_topic_uri(hashcode):
    from api.v1.chat.urls import API_VERSION
    topic = API_VERSION + "/topic/s/{}/".format(hashcode)
    return topic


# def get_config_topic_uri(hashcode):
#     from api.v1.chat.urls import API_VERSION
#     topic = API_VERSION + "/topic/configTopic/{}/".format(hashcode)
#     return topic


def get_leave_topic_uri(hashcode):
    from api.v1.chat.urls import API_VERSION
    topic = API_VERSION + "/topic/leave/{}/".format(hashcode)
    return topic


def get_history_topic_uri(hashcode):
    from api.v1.chat.urls import API_VERSION
    topic = API_VERSION + "/topic/history/{}/".format(hashcode)
    return topic


def get_invite_topic_uri(hashcode):
    from api.v1.chat.urls import API_VERSION
    topic = API_VERSION + "/topic/responseInvite/{}/".format(hashcode)
    return topic


def local2UTC(LocalTime):

    EpochSecond = mktime(LocalTime.timetuple())
    utcTime = datetime.utcfromtimestamp(EpochSecond)
    return utcTime
